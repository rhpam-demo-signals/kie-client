package client;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.kie.server.api.marshalling.MarshallingFormat;
import org.kie.server.client.KieServicesClient;
import org.kie.server.client.KieServicesConfiguration;
import org.kie.server.client.KieServicesFactory;
import org.kie.server.client.ProcessServicesClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

	final static Logger log = LoggerFactory.getLogger(Main.class);

	private static final String URL = "http://localhost:8080/kie-server/services/rest/server";
	private static final String user = System.getProperty("username", "donato");
	private static final String password = System.getProperty("password", "donato");
	private static final String CONTAINER = "signal-process_1.0-SNAPSHOT";
	private static String PROCESS_ID = "src.main.resources.process";


	private KieServicesClient client;

	public static void main(String[] args) {
		try {
			Main clientApp = new Main();
			
			long start = System.currentTimeMillis();

			Long piid = clientApp.launchProcess();
			
			long end = System.currentTimeMillis();
			System.out.println("elapsed time: " + (end - start));

			TimeUnit.SECONDS.sleep(10);
			
			clientApp.sendSignal("external-"+piid, null);

		} catch (InterruptedException e) {
			e.printStackTrace();
		}

	}

	public Main() {
		client = getClient();
	}
	public Long launchProcess() {
		try {
			ProcessServicesClient processClient = client.getServicesClient(ProcessServicesClient.class);
			Map<String, Object> inputData = new HashMap<>();

			// ---------------------------
			return processClient.startProcess(CONTAINER, PROCESS_ID, inputData);

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private void sendSignal(String signalName, Object signalPayload) {
		ProcessServicesClient processClient = client.getServicesClient(ProcessServicesClient.class);
		processClient.signal(CONTAINER, signalName, signalPayload);
	}

	private KieServicesClient getClient() {
		KieServicesConfiguration config = KieServicesFactory.newRestConfiguration(URL, user, password);

		// Configuration for JMS
		//		KieServicesConfiguration config = KieServicesFactory.newJMSConfiguration(connectionFactory, requestQueue, responseQueue, username, password)
		
		// Marshalling
		config.setMarshallingFormat(MarshallingFormat.JSON);
		Set<Class<?>> extraClasses = new HashSet<Class<?>>();
		// add classes eg.
		//		extraClasses.add(OrderInfo.class);
		//		extraClasses.add(SupplierInfo.class);
		config.addExtraClasses(extraClasses);
		Map<String, String> headers = null;
		config.setHeaders(headers);
		KieServicesClient client = KieServicesFactory.newKieServicesClient(config);

		return client;
	}

}
